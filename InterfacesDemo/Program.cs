﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger log = ILoggerFactory.GetLogger();

            log.WriteToLog("This is the log message");
            Console.ReadLine();
        }
    }
}
